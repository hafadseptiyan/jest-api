const { base_url } = require('../../main.js');
require('../../main.js');
const supertest = require('supertest');
const { faker } = require('@faker-js/faker');

async function getUsers(page = 1, per_page = 10) {
    try {
        return await supertest(base_url)
            .get("/users")
            .query({
                page: page,
                per_page: per_page
            })
    } catch (error) {
        console.log(error)
    }
}

async function createUser(payload) {
    try {
        return await supertest(base_url)
            .post("/users")
            .set("Content-Type", "application/json")
            .send(payload)
    } catch (error) {
        console.log(error)
    }
}

async function updateUser(userId, name = undefined, job = undefined) {
    const payload = {
        name: name,
        job: job
    }
    try {
        return await supertest(base_url)
            .put("/users" + "/" + userId)
            .set("Content-Type", "application/json")
            .send(payload)
    } catch (error) {
        console.log(error)
    }
}

async function getDetailUser(userId) {
    try {
        return await supertest(base_url)
            .get("/users" + "/" + userId)
    } catch (error) {
        console.log(error)
    }
}

async function deleteUser(userId) {
    try {
        return await supertest(base_url)
            .delete("/users" + "/" + userId)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getUsers,
    createUser,
    updateUser,
    getDetailUser,
    deleteUser
}