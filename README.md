
## Jest API Automation (Reqres)
1. Create User
2. Update User 
3. Get All and Detail User
4. Delete User

### Usage

```
npm install
```

#### Run Test

```
npm run test
```

## License

[MIT]('')
