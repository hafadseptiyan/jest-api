require('../main.js');
const user      = require('../handlers/users/users.js');
const { faker } = require('@faker-js/faker');

let startTime;

describe("Case: Get users", () => {
    beforeEach(() => {
        startTime = Date.now();
    });

    test("Get list users without parameter", async () => {
        const res = await user.getUsers();

        expect(Date.now() - startTime).toBeLessThan(1600);

        
        expect(res.statusCode).toBe(200);
        expect(res.body.page).toBe(1);
        expect(res.body.data).not.toBe(null);

        expect((res.body.data).length).toBe(10);
    })

    test("Get list of users with per_page", async () => {
        const expectPage    = 2;
        const expectPerPage = 1;
        const res = await user.getUsers(expectPage, expectPerPage);
       
        expect(Date.now() - startTime).toBeLessThan(1600); 
        expect(res.statusCode).toBe(200);
        expect(res.body.page).toBe(expectPage);
        expect((res.body.data).length).toBe(expectPerPage);
    })
})

describe("Case: Create user", () => {
    beforeEach(() => {
        startTime = Date.now()
    })

    test("Create with valid data", async () => {
        const name = faker.person.fullName();
        const job  = faker.person.jobTitle();

        const payload = {
            "name": name,
            "job" : job
        }
        const res = await user.createUser(payload);

        expect(Date.now() - startTime).toBeLessThan(1600);

        expect(res.statusCode).toBe(201);
        expect(JSON.stringify(res.header)).toMatch(/application\/json/i);
        expect(res.body.id).toMatch(/^-?\d+$/);
        expect(res.body.name).toBe(name);
        expect(res.body.job).toBe(job);
        expect(Date.parse(res.body.createdAt)).toBeLessThan(Date.now());
    })
})

describe("Case: Update user", () => {
    test("Update with valid data", async () => {
        const name        = faker.person.fullName();
        const job         = faker.person.jobTitle();
        const createdUser = await user.createUser(name, job);

        startTime = Date.now();

        const updatedRes = await user.updateUser(createdUser.body.id, name, job);
        expect(Date.now() - startTime).toBeLessThan(1600)
        expect(updatedRes.statusCode).toBe(200);
    })

})

describe("Case: Get user detail", () => {
    beforeEach(async () => {
        const res = await user.getUsers()
    })

    test("Get user detail with valid id", async () => {
        const res = await user.getDetailUser(1);
        expect(res.statusCode).toBe(200);
    })
})

describe("Case: Delete user", () => {
    beforeEach(async () => {
        const res    = await user.updateUser(faker.person.fullName(), faker.person.jobTitle());
              userId = res.body.id;
    })

    test("Delete user with valid id", async () => {
        const res = await user.deleteUser(userId);
        expect(res.statusCode).toBe(204);
    })
})